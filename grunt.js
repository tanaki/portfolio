/*global module:false*/
module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: '<json:package.json>',

		depconcat: {
			dist: {
				src: ['dev/src/**/*.js'],
				dest: 'build/js/<%= pkg.namespace %>.js'
			}
		},
		min: {
			dist: {
				src: '<config:depconcat.dist.dest>',
				dest: 'build/js/<%= pkg.namespace %>.min.js'
			}
		},
		lint: {
			files: ['grunt.js', 'dev/src/**/*.js']
		},
		less: {
			dev: {
				src: 'dev/less/style.less',
				dest: 'build/css/style.css'
			},
			deploy: {
				src: 'dev/less/style.less',
				dest: 'build/css/style.css',
				options: {
					compress: true
				}
			}
		},
		watch: {
			files: ['dev/src/**/*.js', 'dev/less/*.less'],
			tasks: 'default'
		}
	});

	// Custom tasks
	grunt.registerTask('default', 'less:dev lint depconcat');
	grunt.registerTask('deploy', 'less:deploy lint depconcat min');

};
